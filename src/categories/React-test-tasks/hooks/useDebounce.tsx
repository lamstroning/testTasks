import {useRef} from "react";

/**
 * Custom hook для задержки вызова функции-обработчика.
 *
 * @param {function} callback - Функция-обработчик.
 * @param {number} delay - Время задержки в миллисекундах.
 * @returns {{ run: function, cancel: function }} - Объект со свойствами:
 *   - `run`: Функция, которая запускает обработчик с задержкой.
 *   - `cancel`: Функция, которая отменяет запланированный вызов обработчика.
 */

export default function useDebounce(callback: Function, delay: number) {
    const debounceRef = useRef<number>()

    const run = () => debounceRef.current = setTimeout(callback, delay)
    const cancel = () => clearTimeout(debounceRef.current)

    return ({run, cancel})
}
