
/**
 * Hook for reading and writing to local storage
 * @param key - key to store data under
 * @param initialValue - initial value to use if no value exists in local storage
 * @returns array with two values: the current value in local storage, and a function to update it
 */
// export default function useLocalStorage<T>(key: string, initialValue: T): [T, (value: T) => void]

/**
 * Hook for checking if a media query matches the current screen size
 * @param query - CSS media query to match
 * @returns boolean indicating whether the current screen size matches the media query
 */
// export default function useMediaQuery(query: string): boolean;

/**
 * Hook for updating state with a delay after user input
 * @param initialValue - initial value for the state
 * @param delay - delay (in ms) before updating the state
 * @returns array with two values: the current debounced state value, and a function to update it
 */
// export default function useDebouncedState<T>(initialValue: T, delay: number): [T, (value: T) => void];

/**
 * Hook for throttling updates to state
 * @param value - value to update
 * @param delay - delay (in ms) before updating the value
 * @returns the throttled value
 */
// export default function useThrottle<T>(value: T, delay: number): T;

