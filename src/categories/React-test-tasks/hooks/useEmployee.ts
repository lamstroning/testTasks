import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {sortByProp} from "../../ts-test-tasks/base";
import {
    deleteEmployee,
    Employee,
    employeeDefault,
    employeeKeys,
    EmployeeProps,
    getEmployeeList, saveEmployeeList,
    updateEmployee
} from "../services/employee";
import {RootState} from "../../../store";
import {setEmployeeList} from "../../../slices/employeeSlice";

const initFilter: Employee = {
    employeeId: '',
    firstName: '',
    lastName: '',
    birthday: '',
    height: '',
}

export function useEmployee() {
    const [editedEmployee, setEditedEmployee] = useState<Employee | undefined>();
    const [loadedList, setLoadedList] = useState<Employee[]>(() => [])
    const [ready, setReady] = useState<boolean>(false)
    const [filter, setFilter] = useState<Employee>(() => initFilter)

    const [sort, setSort] = useState(() => ({
        field: employeeKeys[0],
        asc: false,
    }));

    const employeeList = useSelector(
        (state: RootState) => state.employee.employeeList
    );
    const dispatch = useDispatch();

    const cancelEdit = () => {
        if (editedEmployee?.employeeId === 0)
            dispatch(setEmployeeList(employeeList.filter(item => item.employeeId !== 0)))
        setEditedEmployee(undefined)
    }

    useEffect(() => {
        refreshEmployeeList()
    }, []);

    const refreshEmployeeList = () =>
        getEmployeeList().then((res) => dispatch(setEmployeeList(res))).finally(() => setReady(true))

    const onAddEmployee = () => {
        if (!ready || editedEmployee?.employeeId === 0)
            return
        dispatch(setEmployeeList([...employeeList, employeeDefault]))
        setEditedEmployee(employeeDefault)
    }

    const onEditEmployee = (employee: Employee) => {
        if (employee.employeeId !== editedEmployee?.employeeId) {
            setEditedEmployee(employee)
            return
        }
        if (editedEmployee?.employeeId === 0) {
            setReady(false)
            setLoadedList([...loadedList, editedEmployee])
            return saveEmployeeList(editedEmployee).then(refreshEmployeeList).finally(() => {
                setReady(true)
                cancelEdit()
                setLoadedList([])
            })
        }

        if (editedEmployee) {
            cancelEdit()

            setLoadedList([...loadedList, employee])
            updateEmployee(editedEmployee).then(refreshEmployeeList).finally(() => {
                setLoadedList([])
            })
        }
    };

    const onDeleteEmployee = (employee: Employee, isEdit: boolean) => {
        if (isEdit)
            return cancelEdit()

        setLoadedList([...loadedList, employee])
        deleteEmployee(employee.employeeId).then(refreshEmployeeList).finally(() => setLoadedList([]))
    }

    const onSort = (field: EmployeeProps) => {
        const asc = (field == sort.field && !sort.asc) || false
        dispatch(setEmployeeList(sortByProp(field, employeeList, asc)))
        setSort({ field, asc })
    };

    const onClearFilter = () => setFilter(initFilter)
    return {
        employeeList,
        editedEmployee,
        setEditedEmployee,
        onAddEmployee,
        onEditEmployee,
        onDeleteEmployee,
        onSort,
        sort,
        loadedList,
        setLoadedList,
        setFilter,
        filter,
        onClearFilter
    };
}
