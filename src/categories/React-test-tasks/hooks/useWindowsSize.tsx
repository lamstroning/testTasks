/**
 * Custom hook для автоматического отслеживания изменения размера окна браузера.
 *
 * @returns {{ width: number, height: number }} - Объект с шириной и высотой окна браузера.
 */
import {useEffect, useState} from "react";

export default function useWindowSize() {
    const [windowSize, setWindowSize] = useState(() => ({
        width: window.innerWidth,
        height: window.innerHeight
    }))

    useEffect(() => {
        const handleResize = () =>
            setWindowSize({
                width: window.innerWidth,
                height: window.innerHeight,
            })

        window.addEventListener('resize', handleResize);

        return () => window.removeEventListener('resize', handleResize);
    }, []);

    return (windowSize)
}
