const API_EMPLOYEE = 'https://reactapi.bsite.net/api/Employee'

export type Employee = {
    employeeId: string | number;
    firstName: string;
    lastName: string;
    birthday: string;
    height: string | number;
}

export type EmployeeProps = keyof Employee;

export const employeeDefault: Employee = {
    employeeId: 0,
    firstName: '',
    lastName: '',
    birthday: new Date().toISOString(),
    height: 0
}

export const employeeKeys = Object.keys(employeeDefault) as EmployeeProps[]

export const saveEmployeeList = async (employee: Employee) => {
    try {
        await fetch(`${API_EMPLOYEE}`, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(employee)
        });
    } catch (error) {
        console.error(error);
    }
}

export const updateEmployee = async (employee: Employee) => {
    try {
       await fetch(`${API_EMPLOYEE}`, {
            method: 'put',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(employee)
        });
    } catch (error) {
        console.error(error);
    }
}

export const getEmployeeList = async () => {
    try {
        const response = await fetch(`${API_EMPLOYEE}`, {
            method: 'get',
            headers: {
                'accept': '*/*'
            }
        });
        const data = await response.json();
        return data;
    } catch (error) {
        console.error(error);
    }
}

export const getEmployeeById = async (employeeId) => {
    try {
        const response = await fetch(`${API_EMPLOYEE}/${employeeId}`, {
            method: 'get',
            headers: {
                'accept': '*/*'
            }
        });
        const data = await response.json();
        console.log(data);
    } catch (error) {
        console.error(error);
    }
}
export const deleteEmployee = async (employeeId) => {
    try {
        await fetch(`${API_EMPLOYEE}/${employeeId}`, {
            method: 'delete',
            headers: {
                'accept': '*/*'
            }
        });
    } catch (error) {
        console.error(error);
    }
}
