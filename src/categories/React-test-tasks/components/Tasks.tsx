import React, {useState} from 'react';


/**
 * Тип данных task
 * @typedef {Object} Task - Описание задачи
 * @property {number} id - Идентификатор задачи
 * @property {string} title - Заголовок задачи
 * @property {boolean} completed - Флаг, указывающий, выполнена ли задача
 */
export interface Task {
    id: number;
    title: string;
    completed: boolean;
}

const TASKS_EXAMPLE: Task[] = [
    {
        id: 1,
        title: 'Купить продукты',
        completed: false
    },
    {
        id: 2,
        title: 'Помыть посуду',
        completed: true
    },
    {
        id: 3,
        title: 'Сделать уборку в комнате',
        completed: false
    }
];

/**
 Создает простой список задач с возможностью добавления, удаления и отметки выполнения каждой задачи.
 @param {Array} tasks - Массив задач.
 @param {Function} addTask - Функция для добавления новой задачи в список.
 @param {Function} deleteTask - Функция для удаления задачи из списка.
 @param {Function} toggleCompleteTask - Функция для отметки задачи как выполненной.
 @returns {JSX.Element} - Компонент React, отображающий список задач и форму для их добавления.
 */
function TaskList({tasks, addTask, deleteTask, toggleCompleteTask}) {
    return <div>
        {
            tasks.map(task => <div key={task.id}>
                    <div>
                        <span>{task.title}</span>
                        <span style={{color: "green", fontWeight: 'bold'}}>{task.completed ?  '☑' : '☐'}</span>
                    </div>
                    <button onClick={() => deleteTask(task.id)}>delete</button>
                    <button onClick={() => toggleCompleteTask(task.id)}>
                        {task.completed ? 'not complete' : 'complete'}
                    </button>
                </div>
            )}

        <button onClick={() => addTask({
            id: new Date().getTime(),
            title: 'new Task',
            completed: false})
        }>
            addTask
        </button>
    </div>
}

/**
 *
 * @Result
 */
export function Tasks() {
    const [tasks, setTasks] = useState<Task[]>(TASKS_EXAMPLE)

    const addTask = (newTask: Task) => setTasks([...tasks, newTask])
    const deleteTask = (id: number) => setTasks([...tasks.filter(task => task.id !== id)])
    const toggleCompleteTask = (id: number) => {
        setTasks(
            tasks.map(task => (task.id === id && {...task, completed: !task.completed}) || task)
        )
    }

    return (
        <TaskList
            tasks={tasks}
            addTask={addTask}
            deleteTask={deleteTask}
            toggleCompleteTask={toggleCompleteTask}
        />
    )
}
