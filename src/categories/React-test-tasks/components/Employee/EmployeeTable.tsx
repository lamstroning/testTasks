import React from "react";

import {Employee, employeeKeys} from "../../services/employee";

const EMPLOYEE_KEYS = employeeKeys;

type EmployeeFilterProps = {
    setFilter: (prop: Employee) => void
    filter: Employee,
    onClearFilter: () => void
}

export function EmployeeFilter({setFilter, filter, onClearFilter}: EmployeeFilterProps) {
    return (
        <div className='row'>
            {EMPLOYEE_KEYS.map(prop =>
                <div className='col' key={`filter-${prop}`}>
                    <input
                        placeholder={`filter by ${prop}`}
                        onChange={(event) => {
                            const newFilter = {...filter}
                            newFilter[prop] = event.currentTarget.value
                            setFilter(newFilter)
                        }}
                        value={filter[prop]}
                    />
                </div>
            )}
            <div className='col'>
                <button className='button button_error' onClick={onClearFilter}>
                    Clear filters
                </button>
            </div>
        </div>
    )
}

export function EmployeeHeader({onSort, onAddEmployee, sort}) {
    return (
        <div className='row'>
            {EMPLOYEE_KEYS.map(prop =>
                <div className='col' key={`sort-${prop}`}>
                    <button
                        className='button'
                        onClick={() => onSort(prop)}
                    >
                        {prop}
                        {sort.field === prop && (sort.asc ? '▲' : '▼') }
                    </button>
                </div>
            )}
            <div className='col'>
                <button
                    className='button button_success'
                    onClick={onAddEmployee}
                >
                    {'+ add new employee'}
                </button>
            </div>
        </div>
    )
}

type EmployeeButtonsProp = {
    employee: Employee,
    onEditEmployee: (employee: Employee) => void,
    isEdited: boolean,
    onDeleteEmployee: (employee: Employee, isEdit: boolean) => void,
    loadedList: Employee[],
}

function EmployeeButtons({
    employee,
    isEdited,
    onEditEmployee,
    onDeleteEmployee,
    loadedList
}: EmployeeButtonsProp) {
    const isLoaded = !loadedList.find(item => item.employeeId === employee.employeeId)
    const handleEditEmployee = () => onEditEmployee(employee)
    const handleDeleteEmployee = () => onDeleteEmployee(employee, isEdited)

    const Loading = <button className='button' disabled>...Loading</button>

    return (
        <div className='col'>
            {!isLoaded && Loading || (
                <>
                    <button
                        className='button button_info'
                        onClick={handleEditEmployee}
                        disabled={!isLoaded}
                    >
                        {(isEdited ? 'save' : 'edit')}
                    </button>
                    <button
                        className='button button_error'
                        onClick={handleDeleteEmployee}
                        disabled={!isLoaded}
                    >
                        {isEdited ? 'cancel' : 'delete'}
                    </button>
                </>
            )}
        </div>
    )
}

type EmployeeRowProp = {
    employee: Employee,
    onEditEmployee: (employee: Employee) => void,
    editedEmployee: Employee | undefined
    setEditedEmployee: any,
    onDeleteEmployee: (employee: Employee, isEdit: boolean) => void,
    loadedList: Employee[],
    filter: Employee
}

export function EmployeeRow({
    employee,
    onEditEmployee,
    editedEmployee,
    setEditedEmployee,
    onDeleteEmployee,
    loadedList
}: EmployeeRowProp) {
    const isEdited = employee.employeeId === editedEmployee?.employeeId

    const renderCol = (prop) => {

        const renderEditedCol = () => (editedEmployee &&
            <div key={employee.employeeId + prop} className='col'>
                <input
                    type='text'
                    onChange={(event) => {
                        const newEmployee = {...editedEmployee}
                        newEmployee[prop] =  event.currentTarget.value
                        setEditedEmployee(newEmployee)
                    }}
                    value={editedEmployee[prop]}
                    disabled={prop === 'employeeId'}
                />
            </div>
        )

        const renderUsualCol = () => (
            <div key={employee.employeeId + prop} className='col'>{employee[prop]}</div>
        )

        return isEdited ? renderEditedCol() : renderUsualCol()
    }

    return (
        <div className='row' key={employee.employeeId}>
            {EMPLOYEE_KEYS.map(renderCol)}
            <EmployeeButtons
                employee={employee}
                isEdited={isEdited}
                onEditEmployee={onEditEmployee}
                onDeleteEmployee={onDeleteEmployee}
                loadedList={loadedList}
            />
        </div>
    )
}
