import React from "react";
import {EmployeeFilter, EmployeeHeader, EmployeeRow} from "./EmployeeTable";
import {useEmployee} from "../../hooks/useEmployee";
import {Employee, employeeKeys} from "../../services/employee";

const EMPLOYEE_KEYS = employeeKeys;

export function EmployeeComponent() {
    const {employeeList} = useEmployee()
    const {
        onSort,
        sort,
        onAddEmployee,
        onEditEmployee,
        editedEmployee,
        setEditedEmployee,
        onDeleteEmployee,
        loadedList,
        setFilter,
        filter,
        onClearFilter,
    } = useEmployee()

    const checkFilter = (employee: Employee, filter: Employee): boolean =>
        Object.keys(employee).every(key =>
        filter[key].length === 0 ||
        filter.hasOwnProperty(key) &&
        employee[key].toString().includes(filter[key])
    );

    return (
        <div>
            <EmployeeHeader
                onSort={onSort}
                onAddEmployee={onAddEmployee}
                sort={sort}
            />
            <EmployeeFilter setFilter={setFilter} filter={filter} onClearFilter={onClearFilter}/>

            {employeeList.map((employee) =>
                checkFilter(employee, filter) &&
                <EmployeeRow
                    key={employee.employeeId}
                    employee={employee}
                    onEditEmployee={onEditEmployee}
                    editedEmployee={editedEmployee}
                    setEditedEmployee={setEditedEmployee}
                    onDeleteEmployee={onDeleteEmployee}
                    loadedList={loadedList}
                    filter={filter}
                />
            )}
        </div>
    )
}
