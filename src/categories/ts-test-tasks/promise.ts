
// написать функцию, которая возвращает промис
// который будет выполнен через указанное количество миллисекунд.

function delay(time: number, callback: () => void): Promise<any> {
    return new Promise((resolve, reject) =>
        setTimeout(() => {
            try {
                callback()
                resolve('success')
            } catch {
                reject()
            }
        }, time))
}
