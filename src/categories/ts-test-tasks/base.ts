/**
 * Функция принимает два аргумента: массив чисел и число,
 * возвращает массив индексов элементов, которые равны заданному числу.
 *
 * @template T
 * @param {number[]} arr - Массив элементов, в котором ищем числа
 * @param {number} num - Число, которое ищем
 * @returns {number[]} - Массив индексов элементов, равных заданному числу
 */

const findIndexes = (arr: number[], num: number): number[] =>
    arr.reduce((indexes: number[], item, index) => {
        item === num && indexes.push(index)
        return indexes
    }, []);

// Пример использования функции:
/*
const arr = [1, 2, 3, 2, 4, 2, 5]
const num = 2
console.log(findIndexes(arr, num)) // [1, 3, 5]
*/


/**
 * Функция принимает массив любого типа и возвращает его в обратном порядке.
 *
 * @template T
 * @param {T[]} arr - Массив, который необходимо развернуть.
 * @returns {T[]} - Развернутый массив.
 */
const reverseArray = <T>(arr: T[]): T[] => arr.reverse();

/**
 * Функция фильтрует массив объектов по заданному значению свойства.
 *
 * @template T
 * @param {T[]} arr - Массив объектов.
 * @param {keyof T} prop - Название свойства объекта.
 * @param {T[keyof T]} value - Значение, по которому нужно отфильтровать объекты.
 * @returns {T[]} - Новый массив объектов, отфильтрованный по заданному свойству.
 */
const filterByProperty = <T>(arr: T[], prop: keyof T, value: T[keyof T]): T[] =>
    arr.filter((item: T) => item[prop] === value)
/*
Пример вызова:

interface Person {
    name: string;
    age: number;
}

const people: Person[] = [
    { name: 'Alice', age: 25 },
    { name: 'Bob', age: 30 },
    { name: 'Charlie', age: 35 },
    { name: 'David', age: 40 }
];

filterByProperty(people, 'age', 30) // [ { name: 'Bob', age: 30 } ]
*/
/**
 * Функция преобразует массив объектов в объект, используя значение заданного свойства в качестве ключа.
 *
 * @template T
 * @param {T[]} arr - Массив объектов.
 * @param {keyof T} prop - Название свойства объекта, которое нужно использовать в качестве ключа.
 * @returns {{ [key: string]: T }} - Объект, полученный из массива.
 */
const arrayToObject = <T>(arr: T[], prop: keyof T): {[key: string]: T} =>
    arr.reduce((obj, item) => {
        const key = item[prop] as unknown as string
        obj[key] = item
        return obj
    }, {})
/*

const people = [
    { name: "Alice", age: 25 },
    { name: "Bob", age: 30 },
    { name: "Charlie", age: 35 },
];

const peopleByName = arrayToObject(people, "name");

console.log(peopleByName);

*/

/**
 * Напишите функцию, которая принимает массив объектов и название свойства
 * в качестве аргументов и возвращает новый объект
 * в котором ключами являются значения указанного свойства
 * а значениями массивы объектов с этим значением свойства
 *
 * @template T
 * @param{T[]} arr - массив объектов
 * @param{keyof T} prop - Название свойства
 * @return{{[key: string]: T}} - Объект, полученный из массива.
 *
 */

const groupByProp = <T>(arr: T[], prop: keyof T): {[key: string]: T[]} =>
    arr.reduce((obj, item) => {
        const key = item[prop] as unknown as string
        obj[key] ? obj[key] = [item] : obj[key].push(item)

        return obj
    }, {})


/**
 * Функция, которая принимает аргумент любого типа и возвращает строку,
 * указывающую на тип переданного аргумента.
 *
 * @param {unknown} arg - Аргумент любого типа.
 * @returns {string} - Строка, указывающая на тип переданного аргумента.
 */

const getType = (arg: unknown): string => typeof arg

/**
 * Функция, которая принимает массив объектов и ключ объекта, и возвращает массив
 * значений данного ключа из каждого объекта.
 *
 * @param {T[]} arr - Массив объектов.
 * @param {K} key - Ключ объекта, значения которого нужно извлечь.
 * @returns {Array<T[K]>} - Массив значений ключа из каждого объекта.
 */

const getArrayKeys = <T, K extends keyof T>(arr: T[], key: K): Array<T[K]> =>
    arr.map((item) => item[key])

/**
 * Функция принимает другую функцию и выводит ее имя в виде строки.
 *
 * @param {{Function} func} - Функция
 * @return {string} вернем название функции
 */
const getFunctionName = (func: Function): string => func.name

/*
function helloWorld() {
    console.log('Hello, World!');
}

console.log(getFunctionName(helloWorld)); // выведет "helloWorld"
*/

export const sortByProp = <T>(propName: keyof T, arr: T[], asc: boolean = true) =>
    arr.slice().sort((a, b) => {
        const direction = asc ? 1 : -1
        return (a[propName] > b[propName] && direction) || (a[propName] < b[propName] && -1 * direction) || 0
    });
