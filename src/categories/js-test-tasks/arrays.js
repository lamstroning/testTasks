

// Напишите функцию, которая принимает массив чисел и возвращает сумму всех элементов массива.

const sumArray = (array) =>
    array.reduce((accumulator, currentValue) => accumulator + currentValue)

// sumArray([1, 2, 3, 4])

// Напишите функцию, которая принимает два массива и возвращает новый массив, содержащий элементы, которые есть только в одном из массивов.
const uniqueElements = (arr1, arr2) =>
    arr1.map(item => arr2.find((item2) => item === item2)).filter(Boolean)

// console.log(uniqueElements([1, 2, 4, 4, 3], [1, 3]))


// Напишите функцию, которая принимает массив строк и возвращает новый массив, состоящий из тех же строк, но с заглавной буквы в начале каждой строки.
const capitalize = (arr) => arr.map(str => `${str[0].toUpperCase()}${str.slice(1)}`)

// Напишите функцию, которая принимает массив объектов, каждый из которых представляет пользователя с полями name и age. Функция должна возвращать массив имен всех пользователей, которые старше 18 лет.
const getAdult = (arr) => arr.filter((item) => item.age > 18)

// Напишите функцию, которая принимает два массива чисел и возвращает новый массив, состоящий из всех элементов этих двух массивов, но без повторений.
const getUniqNumbers = (arr1, arr2) => [...new Set(arr1), ...new Set(arr2)]

// Напишите функцию, которая принимает массив чисел и возвращает наименьшее число в этом массиве.
const getMin = (arr) => Math.min(...arr)

// Напишите функцию, которая принимает массив чисел и возвращает новый массив, состоящий из тех же чисел, но упорядоченных по возрастанию.
const orderArray = (arr) => arr.sort((a, b) => a - b)


// Напишите функцию sortObjectArray, которая принимает массив объектов вида { name: 'John', age: 25 }
// и возвращает новый массив, отсортированный по возрасту от меньшего к большему.
const sortObjectArray = (arr) => arr.sort((a, b) => b.age - a.age)

// Напишите функцию groupBy, которая принимает массив объектов и строку с названием свойства объекта
// и возвращает объект, где ключами являются уникальные значения этого свойства, а значениями — массивы объектов, у которых это свойство имеет такое значение. Например, для массива [{ name: 'John', age: 25 }, { name: 'Mary', age: 30 }, { name: 'John', age: 35 }] и свойства name функция должна вернуть { John: [{ name: 'John', age: 25 }, { name: 'John', age: 35 }], Mary: [{ name: 'Mary', age: 30 }] }.
const groupBy = (arr, objName) => {
    new Set()
}
const getTotalAge = (arr) => arr.reduce((currentValue, prevValue) => currentValue.age + prevValue.age)

// const arr = [{ name: 'Alice', age: 25 },  { name: 'Bob', age: 30 },  { name: 'Charlie', age: 20 }];
const getOldestPerson = (arr) => {
    let maxAge = Math.max(...arr.map(item => item.age))
    return arr.filter(item => item.age === maxAge)
}


/*
[
	{ name: 'Alice', age: 25, city: 'NY' },
	{ name: 'Bob', age: 30, city: 'LA' },
	{ name: 'Charlie', age: 35, city: 'LA' },
	{ name: 'Dave', age: 40, city: 'NY' }
]
*/

// написать функцию groupObjectsByKey, которая принимает массив объектов и ключ
// по которому нужно сгруппировать эти объекты. Функция должна возвращать объект
// где ключи - уникальные значения, найденные в свойстве объектов, указанном ключе.
// Значения - массив объектов, которые содержат этот ключ и значение для каждого уникального значения.

// @input
//[
//   { name: 'Alice', age: 25, city: 'NY' },
//   { name: 'Bob', age: 30, city: 'LA' },
//   { name: 'Charlie', age: 35, city: 'LA' },
//   { name: 'Dave', age: 40, city: 'NY' }
// ]

// @output
//{
//   'NY': [
//     { name: 'Alice', age: 25, city: 'NY' },
//     { name: 'Dave', age: 40, city: 'NY' }
//   ],
//   'LA': [
//     { name: 'Bob', age: 30, city: 'LA' },
//     { name: 'Charlie', age: 35, city: 'LA' }
//   ]
// }

const input = [
    { name: 'Alice', age: 25, city: 'NY' },
    { name: 'Bob', age: 30, city: 'LA' },
    { name: 'Charlie', age: 35, city: 'LA' },
    { name: 'Dave', age: 40, city: 'NY' }
]
const key = 'city'

const groupObjectsByKey = (array, key) => {
    const res = {}

    array.map(item => {
        const groupKey = item[key]
        if (!res[groupKey])
            res[groupKey] = []
        res[groupKey]?.push(item);
    })
    return res
}

// groupObjectsByKey(input, key)

// Найти сумму элементов массива, которые являются четными числами

const sumEvenNumbers = (arr) => arr.reduce((result, item) => result += (item % 2 === 0) ? item : 0, 0)
/*
    const sumEvenNumbersArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    console.log(sumEvenNumbers(sumEvenNumbersArr)); // 30
*/

const findDuplicates = (arr) => arr.reduce((result, item) => {
    if (arr.indexOf(item) !== arr.lastIndexOf(item) && !result.find(num => num === item))
        result.push(item)
    return result
}, [])
/*
const findDuplicatesArr = [1, 2, 3, 4, 5, 2, 4, 6, 7, 8, 5, 9];
console. log(findDuplicates(findDuplicatesArr)); // [2, 4, 5]
*/
