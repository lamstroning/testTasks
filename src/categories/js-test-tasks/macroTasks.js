
// Написать функцию, которая выводит числа от 1 до 5 в консоль каждую секунду с помощью функции setTimeout.

function counter() {
    let counter = 0

    const add = () => setTimeout(() => counter++ < 5 && add() && console.log(counter), 100)
    add()
}

// counter()

//Напишите функцию sayHello, которая будет выводить в консоль
// строку "Hello, World!" определенное количество раз
// с заданным интервалом времени в 1 секунду.
// Функция должна принимать необязательный параметр count
// который указывает, сколько раз нужно вывести сообщение в консоль.


function sayHello(count = 0) {
    const intervalId = setInterval(() => !console.log("Hello, World!") && --count <= 0 && clearInterval(intervalId), 100)
}

// sayHello(5)
