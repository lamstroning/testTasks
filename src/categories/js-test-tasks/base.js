/**
 * Функция для переворота строки
 * @param {string} str - строка для переворота
 * @returns {string} перевернутая строка
 */
const reverseString = (str) => str.split('').reverse().join('')

// Пример использования
// console.log(reverseString('hello')); // 'olleh'

/**
 * Функция для строки в Capitalize
 * @param {string} str - строка для преобразования
 * @returns {string} строка в формате camelCase
 */
const toCapitalize = (str) => `${str[0].toUpperCase()}${str.slice(1)}`

// Пример использования
// console.log(toCapitalize('this is a test')); // 'thisIsATest'


/**
 * Функция для удаления гласных из строки для русских и английских букв
 * @param {string} str - строка для обработки
 * @returns {string} строка без гласных букв
 *
 * массивы из гласных букв
 * russian: ['а', 'е', 'ё', 'и', 'о', 'у', 'ы', 'э', 'ю', 'я'],
 * english: ['a', 'e', 'i', 'o', 'u']
 */

const removeVowels = (str) => {
    const vowels = {
        russian: ['а', 'е', 'ё', 'и', 'о', 'у', 'ы', 'э', 'ю', 'я'],
        english: ['a', 'e', 'i', 'o', 'u']
    }

    str.replace(/vowels/)

}

// Пример использования
console.log(removeVowels('hello world, привет мир ')) // 'hll wrld'
