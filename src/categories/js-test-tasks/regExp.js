// 1. Напишите регулярное выражение, которое будет проверять обязательное наличие в строке символов @ и .

const isEmailValid = (email) => /@.*\./.test(email)
/*
console.log(isEmailValid('example@mail.com')); // true
console.log(isEmailValid('example@mailcom')); // false
*/
// 2. Напишите регулярное выражение, которое будет проверять является ли строка валидным номером телефона.
const isPhoneNumberValid = (phoneNumber) =>  /^\+\d{9,}$/.test(phoneNumber)

/*
console.log(isPhoneNumberValid('+79026222222')); // true
console.log(isPhoneNumberValid('79222222')); // false
console.log(isPhoneNumberValid('+79222222')); // false
*/
