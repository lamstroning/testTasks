
// Создать промис, который после задержки в 1 секунду вернет число 42
new Promise((resolve) => setTimeout(() => resolve(42), 1000)).then(console.log)

// Создать функцию, которая принимает число и возвращает промис, который будет разрешен, если число четное, и будет отклонен, если число нечетное:
const isOdd = (number) =>
    new Promise((resolve, reject) => number % 2 ? reject('is Odd') : resolve('is even'))

// isOdd(11).then(console.log).catch(console.error)

// Создать функцию, которая принимает два промиса и возвращает новый промис, который разрешится с массивом результатов, когда оба промиса будут завершены:


const firstPromise = new Promise(resovle => resovle('firstPromis'))
const secondPromise = new Promise(resovle => resovle('secondPromise'))

const joinPromises = (firstPromise, secondPromise) => Promise.all([firstPromise, secondPromise])


// joinPromises(firstPromis, secondPromise).then(console.log)

// Создать функцию, которая принимает массив чисел и возвращает новый промис, который разрешится с массивом квадратов чисел из исходного массива:

const squareNumbers = (array) => new Promise(resolve => resolve(array.map(n => n ** 2)))

// squareNumbers([1, 2, 3]).then(console.log)

// Создать функцию, которая делает запрос на сервер и возвращает промис, который разрешится с результатом ответа сервера:


const getRequest = () => fetch('https://jsonplaceholder.typicode.com/todos/1').then(console.log).catch(console.error)

// console.log(getRequest())
