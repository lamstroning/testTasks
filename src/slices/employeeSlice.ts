import { createSlice } from '@reduxjs/toolkit';
import {Employee} from "../categories/React-test-tasks/services/employee";

export const employeeSlice = createSlice({
    name: 'employee',
    initialState: {
        employeeList: [] as Employee[]
    },
    reducers: {
        setEmployeeList: (state, action) => {
            state.employeeList = action.payload
        },
    },
})

export const {setEmployeeList} = employeeSlice.actions
