import React from 'react';
import { Provider } from 'react-redux';
import {store} from "./store";
import {EmployeeComponent} from "./categories/React-test-tasks/components/Employee/Employee";

export default function App() {
    return (
        <>
            <Provider store={store}>
                <EmployeeComponent />
            </Provider>
        </>
  );
}
